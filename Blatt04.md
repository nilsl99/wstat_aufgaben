# Blatt 04
*Nils Liebreich*

## Aufgabe 1
### (a)
Sei $\Omega$ ein Laplace-Raum mit folgenden Elementarereignissen
+ `123` alle kriegen ihre eigenen Jacken
+ `132` nur Luca kriegt seine eigene Jacke (Mika's und Noa's Jacken sind vertauscht)
+ `321` nur Mika kriegt ihre eigene Jacke (Luca's und Noa's Jacken sind vertauscht)
+ `213` nur Noa kriegt seine eigene Jacke (Luca's und Mika'sJacken sind vertauscht)
+ `231` keiner kriegt seine eigene Jacke, wobei jeder die Jacke von seinem rechten Nachbarn kriegt
+ `312` keiner kriegt seine eigene Jacke, wobei jeder die Jacke von seinem linken Nachbarn kriegt

Dann gilt

$$\begin{aligned}
p(\omega) &= \frac{1}{|\Omega|} = \frac{1}{6} \\
\Longrightarrow P(A) &= \sum_{\omega \in A} p(\omega) \\
&= \frac{|A|}{6}
\end{aligned}$$

### (b)
Für die Wahrscheinlichkeit $P_b$, dass keiner seine eigene Jacke bekommt, gilt:
$$\begin{aligned}
P_b &= P(\{231, 312\}) \\
&= \frac{|\{231, 312\}|}{6} \\
&= \frac{2}{6} = \frac{1}{3}
\end{aligned}$$

### (c)
Für die Wahrscheinlichkeit $P_c$, dass genau eine Person ihre eigene Jacke bekommt, gilt:
$$\begin{aligned}
P_c &= P(\{132, 321, 213\}) \\
&= \frac{|\{132, 321, 213\}|}{6} \\
&= \frac{3}{6} = \frac{1}{2}
\end{aligned}$$



## Aufgabe 2
Wir definieren mehrere Ereignisse
$$\begin{aligned}
X &\hat{=} \text{ Beide Kinder sind weiblich} \\
A &\hat{=} \text{ Das ältere Kind ist weiblich} \\
B &\hat{=} \text{ Das jüngere Kind ist weiblich} \\
C &\hat{=} \text{ Mind. ein Kind ist weiblich} \\
D &\hat{=} \text{ Eines der beiden Kinder ist weiblich}
\end{aligned}$$
Für die Wahrscheinlichkeit von $X$ gilt:
$$P(X) = P(A) \cdot P(B)$$

### (a)
Für die Wahrscheinlichkeit $P_a$, dass beide Kinder weiblich sind, wenn wir wissen, dass das ältere Kind weiblich ist gilt:
$$P_a = P(X|A)$$
Dort können wir nun den Satz von Bayes anwenden.
$$\begin{aligned}
P_a &= \frac{P(A|X) \cdot P(X)}{P(A)} \\
&= \frac{P(A|X) \cdot P(A) \cdot P(B)}{P(A)} \\
&= P(A|X) \cdot P(B) \\
&= 1 \cdot \frac{1}{2} = \frac{1}{2}
\end{aligned}$$

### (b)
Für die Wahrscheinlichkeit $P_b$, dass beide Kinder weiblich sind, wenn wir wissen, dass mind. ein Kind weiblich ist, gilt:
$$P_b = P(X|C)$$
Dort können wir nun den Satz von Bayes anwenden.
$$\begin{aligned}
P_b &= \frac{P(C|X) \cdot P(X)}{P(C)} \\
&= \frac{P(C|X) \cdot P(A) \cdot P(B)}{1 - [1 - P(A)] \cdot [1 - P(B)]} \\
&= \frac{1 \cdot \frac{1}{2} \cdot \frac{1}{2}}{1 - \left(\frac{1}{2} \cdot \frac{1}{2}\right)} \\
&= \frac{\frac{1}{4}}{\frac{3}{4}} \\
&= \frac{1}{3}
\end{aligned}$$

### (c)
Wenn wir davon ausgehen, dass Alexis und Chris unabhängig voneinander für eines ihrer beiden Kinder entschieden haben, dann ist die Wahrscheinlichkeit, dass Alexis das ältere Kind genommen hat, $\frac{1}{2}$ und die Wahrscheinlichkeit, dass Chris das ältere Kind genommen hat, ebenfalls $\frac{1}{2}$. Um die Wahrscheinlichkeit zu bestimmen, dass beide Kinder weiblich sind, können wir nun einfach alle möglichen Fälle aufschreiben und dann Zählen welche möglich sind und bei wie vielen, beide Kinder weiblich sind.

| 1.Kind | 2. Kind | Wahl des Vaters | Wahl der Mutter | Bei der obigen Aussage noch möglich |
| - | - | - | - | - |
| ♂ | ♂ | 1. Kind | 1. Kind | nein
| ♂ | ♂ | 1. Kind | 2. Kind | nein
| ♂ | ♂ | 2. Kind | 1. Kind | nein
| ♂ | ♂ | 2. Kind | 2. Kind | nein
| ♂ | ♀ | 1. Kind | 1. Kind | nein
| ♂ | ♀ | 1. Kind | 2. Kind | nein
| ♂ | ♀ | 2. Kind | 1. Kind | nein
| ♂ | ♀ | 2. Kind | 2. Kind | ja
| ♀ | ♂ | 1. Kind | 1. Kind | ja
| ♀ | ♂ | 1. Kind | 2. Kind | nein
| ♀ | ♂ | 2. Kind | 1. Kind | nein
| ♀ | ♂ | 2. Kind | 2. Kind | nein
| ♀ | ♀ | 1. Kind | 1. Kind | ja
| ♀ | ♀ | 1. Kind | 2. Kind | ja
| ♀ | ♀ | 2. Kind | 1. Kind | ja
| ♀ | ♀ | 2. Kind | 2. Kind | ja

In den untersten vier Fällen sind beide Kinder weiblich, somit ist die Wahrscheinlichkeit $P_c$, dass beide Kinder weiblich sind gleich:
$$P_c = \frac{4}{6} = \frac{2}{3}$$

## Aufgabe 3
Für die Wahrscheinlichkeit $p$ gilt

$$p(k) = \frac{1}{6} \cdot \left(\frac{5}{6}\right)^{k-1}$$
da wir die ersten $k$-Male keine 6 würfeln und dann beim $k$-ten Mal eine 6 würfeln.

Die Wahrscheinlichkeit $P_2$ , dass bei einem geraden Anzahl an Würfen das erste Mal eine 6 gewürfelt wird ist gegeben durch die Summe aller $p(k)$ für alle geraden $k$

$$\begin{aligned}
P_2 &= \sum_{i=1}^\infty p(2i) \\
&= \sum_{i=1}^\infty \frac{1}{6} \cdot \left(\frac{5}{6}\right)^{2i - 1} \\
&= \frac{5}{36} \cdot \sum_{i=0}^\infty \left(\frac{5^2}{6^2}\right)^i \\
&= \frac{5}{36} \cdot \frac{1}{1-\frac{25}{36}} \\
&= \frac{5}{36} \cdot \frac{36}{11} = \frac{5}{11}
\end{aligned}$$
